import phoenixAES as phAES
import aeskeyschedule
import numpy as np
import sys, os

type_1_temp = [0, 7, 10, 13]
type_2_temp = [1, 4, 11, 14]
type_3_temp = [2, 5,  8, 15]
type_4_temp = [3, 6,  9, 12]

type_temp = [type_1_temp, type_2_temp, type_3_temp, type_4_temp]

two_times = [  0,   2,   4,   6,   8,  10,  12,  14,  16,  18,  20,  22,  24,  26,  28,  30,  32,  34,
              36,  38,  40,  42,  44,  46,  48,  50,  52,  54,  56,  58,  60,  62,  64,  66,  68,  70,
              72,  74,  76,  78,  80,  82,  84,  86,  88,  90,  92,  94,  96,  98, 100, 102, 104, 106,
             108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140, 142,
             144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178,
             180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, 202, 204, 206, 208, 210, 212, 214,
             216, 218, 220, 222, 224, 226, 228, 230, 232, 234, 236, 238, 240, 242, 244, 246, 248, 250,
             252, 254,  27,  25,  31,  29,  19,  17,  23,  21,  11,   9,  15,  13,   3,   1,   7,   5,
              59,  57,  63,  61,  51,  49,  55,  53,  43,  41,  47,  45,  35,  33,  39,  37,  91,  89,
              95,  93,  83,  81,  87,  85,  75,  73,  79,  77,  67,  65,  71,  69, 123, 121, 127, 125,
             115, 113, 119, 117, 107, 105, 111, 109,  99,  97, 103, 101, 155, 153, 159, 157, 147, 145,
             151, 149, 139, 137, 143, 141, 131, 129, 135, 133, 187, 185, 191, 189, 179, 177, 183, 181,
             171, 169, 175, 173, 163, 161, 167, 165, 219, 217, 223, 221, 211, 209, 215, 213, 203, 201,
             207, 205, 195, 193, 199, 197, 251, 249, 255, 253, 243, 241, 247, 245, 235, 233, 239, 237,
             227, 225, 231, 229]

three_times =[  0,   3,   6,   5,  12,  15,  10,   9,  24,  27,  30,  29,  20,  23,  18,  17,  48,  51,
               54,  53,  60,  63,  58,  57,  40,  43,  46,  45,  36,  39,  34,  33,  96,  99, 102, 101,
              108, 111, 106, 105, 120, 123, 126, 125, 116, 119, 114, 113,  80,  83,  86,  85,  92,  95,
               90,  89,  72,  75,  78,  77,  68,  71,  66,  65, 192, 195, 198, 197, 204, 207, 202, 201,
              216, 219, 222, 221, 212, 215, 210, 209, 240, 243, 246, 245, 252, 255, 250, 249, 232, 235,
              238, 237, 228, 231, 226, 225, 160, 163, 166, 165, 172, 175, 170, 169, 184, 187, 190, 189,
              180, 183, 178, 177, 144, 147, 150, 149, 156, 159, 154, 153, 136, 139, 142, 141, 132, 135,
              130, 129, 155, 152, 157, 158, 151, 148, 145, 146, 131, 128, 133, 134, 143, 140, 137, 138,
              171, 168, 173, 174, 167, 164, 161, 162, 179, 176, 181, 182, 191, 188, 185, 186, 251, 248,
              253, 254, 247, 244, 241, 242, 227, 224, 229, 230, 239, 236, 233, 234, 203, 200, 205, 206,
              199, 196, 193, 194, 211, 208, 213, 214, 223, 220, 217, 218,  91,  88,  93,  94,  87,  84,
               81,  82,  67,  64,  69,  70,  79,  76,  73,  74, 107, 104, 109, 110, 103, 100,  97,  98,
              115, 112, 117, 118, 127, 124, 121, 122,  59,  56,  61,  62,  55,  52,  49,  50,  35,  32,
               37,  38,  47,  44,  41,  42,  11,   8,  13,  14,   7,   4,   1,   2,  19,  16,  21,  22,
               31,  28,  25,  26]

_AesInvSBox = [
0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,#0f
0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,#1f
0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,#2f
0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,#3f
0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
]

def iS(byte):
    return _AesInvSBox[byte]

#for testing purpouses, using a prebuilt library
import phoenixAES as phAES
def phoenix(golden_ct, faulty_ct_arr, verbose = 1):
    if verbose == 0 : sys.stdout = open(os.devnull, 'w')  #even with verbose=0 the library prints its output
    key = phAES.crack_bytes(faulty_ct_arr, golden_ct, encrypt=True, verbose=verbose)
    sys.stdout = sys.__stdout__
    return key

def byte_xor(ba1, ba2):
    return bytes([_a ^ _b for _a, _b in zip(ba1, ba2)])

def number_of_different_bytes(ba1, ba2):
    sum = 0
    for i in range(len(ba1)):
        if ba1[i] != ba2[i]:
            sum += 1 
    return sum

def faulty_bytes(faulty_ct, golden_ct):
    diff = []
    assert(len(faulty_ct) == len(golden_ct))
    for i in range(len(faulty_ct)):
        if faulty_ct[i] != golden_ct[i]:
            diff.append(i)
    return diff

def get_type(golden_ct, faulty_ct):
    if faulty_bytes(golden_ct, faulty_ct) == type_1_temp : return 0
    if faulty_bytes(golden_ct, faulty_ct) == type_2_temp : return 1
    if faulty_bytes(golden_ct, faulty_ct) == type_3_temp : return 2
    if faulty_bytes(golden_ct, faulty_ct) == type_4_temp : return 3

    return -1

def DFA(golden_ct, faulty_ct_arr, verbose = 1):
    k_hyp = [set(), set(), set(), set()]
    checked = set()

    key = [None]*16
    found = [False, False, False, False]

    for faulty_ct in faulty_ct_arr:
        
        
        #print(byte_xor(faulty_ct, golden_ct).hex())
        if number_of_different_bytes(faulty_ct, golden_ct) != 4 :
            if verbose > 5 : print(faulty_ct.hex(), "     ", byte_xor(faulty_ct, golden_ct).hex(), "type: -1")
            if verbose > 5 : print("Skipping, reason: wrong difference")
            continue

        text_type = get_type(golden_ct, faulty_ct)
        if verbose > 1 : print(faulty_ct.hex(), "     ", byte_xor(faulty_ct, golden_ct).hex(), "type: ", text_type)

        if faulty_ct in checked : 
            if verbose > 4 :print("Skipping, reason: allready checked")
            continue
        if found[text_type]: 
            if verbose > 4 :print("Skipping, reason:  key byte found")
            continue

        for f in range(1, 256):
            for k0 in range(256): #two
                if not found[text_type] and two_times[f] == (iS((golden_ct[type_temp[text_type][0]]^k0)) ^ iS((faulty_ct[type_temp[text_type][0]]^k0))):
                    for k1 in range(256):
                        if not found[text_type] and f == iS(golden_ct[type_temp[text_type][3]]^k1)^iS(faulty_ct[type_temp[text_type][3]]^k1):
                            for k2 in range(256):
                                if not found[text_type] and f == (iS(golden_ct[type_temp[text_type][2]]^k2)^iS(faulty_ct[type_temp[text_type][2]]^k2)):
                                    for k3 in range(256): #three
                                        if not found[text_type] and three_times[f] == (iS(golden_ct[type_temp[text_type][1]]^k3)^iS(faulty_ct[type_temp[text_type][1]]^k3)):
                                            if (k0, k3, k2, k1) in k_hyp[text_type]:
                                                key[type_temp[text_type][0]] = k0
                                                key[type_temp[text_type][3]] = k1
                                                key[type_temp[text_type][2]] = k2
                                                key[type_temp[text_type][1]] = k3
                                                found[text_type] =True
                                                if verbose > 0 : print("Uncovered new key bytes for type: ", text_type)
                                                if verbose > 0 : print(''.join(["%02X" % x if x is not None else "__" for x in key]))
                                            else:
                                                k_hyp[text_type].add((k0, k3, k2, k1))
        
        checked.add(faulty_ct)
    return key

def load_file(filename):
    """
    Loads unfaulted cyphertext and faulty cyphertexts from file specified by filename.
    
    Expected format for the file is a .npy file with the unfauled ct at the first position and faulty cts afterwards.
    :param filename: Path to file containing unfaulted ct and faulted cts
    :returns: a tupple of the unfaulted ct and a list of faulty cts
    """
    file = open(filename, "rb")
    ct_arr = np.load(file)
    return ct_arr[0], ct_arr[1:]

if __name__ == '__main__':
    #load golden_ct and faulty cyphertexts
    golden_ct, faulty_ct_arr = load_file(sys.argv[1])

    #print(golden_ct)
    #print(faulty_ct_arr[0])

    key_m =DFA(golden_ct, faulty_ct_arr, 4)

    
    key_ph = bytes.fromhex(phoenix(golden_ct, faulty_ct_arr, 0))

    print("10th round key:")
    print("Key found :   ", ''.join(["%02X" % x if x is not None else "__" for x in key_m]))
    print("Key ref   :   ", ''.join(["%02X" % x if x is not None else "__" for x in key_ph]))

    #use a premade library to reverse the key schedule
    print("Original key: ", aeskeyschedule.reverse_key_schedule(key_ph, 10).hex())
    

    